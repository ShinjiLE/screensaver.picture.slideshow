# Kodi Media Center language file
# Addon Name: Picture Slideshow Screensaver
# Addon id: screensaver.picture.slideshow
# Addon Provider: Team-Kodi
msgid ""
msgstr ""
"Project-Id-Version: XBMC Addons\n"
"Report-Msgid-Bugs-To: translations@kodi.tv\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"PO-Revision-Date: 2021-08-03 14:29+0000\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: Hebrew (Israel) <https://kodi.weblate.cloud/projects/kodi-add-ons-look-and-feel/screensaver-picture-slideshow/he_il/>\n"
"Language: he_il\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Weblate 4.7.2\n"

msgctxt "Addon Summary"
msgid "Screensaver that displays an image slideshow"
msgstr "שומר מסך אשר יציג מצגת של תמונות"

msgctxt "Addon Description"
msgid "The Slideshow screensaver will show you a slide show of images using various transition effects. It can be configured to show your libraries music or video fanart, or a custom folder of images"
msgstr "השומר מסך יציג לך מצגת של תמונות אשר עושה שימוש במעברים שונים. יש אפשרות להגדיר את תמונות הפאנארט של ספריות המוזיקה והוידאו, או תיקיית תמונות אישית"

msgctxt "#30000"
msgid "Source of slideshow images"
msgstr "מקור תמונות המצגת"

msgctxt "#30001"
msgid "Folder"
msgstr "תיקיה"

msgctxt "#30002"
msgid "Video Fanart"
msgstr "וידאו פאנארט"

msgctxt "#30003"
msgid "Music Fanart"
msgstr "מוזיקה פאנארט"

msgctxt "#30004"
msgid "Image Folder"
msgstr "תיקיית תמונות"

msgctxt "#30005"
msgid "Dim level"
msgstr "רמת עמעום"

msgctxt "#30006"
msgid "Effect"
msgstr "אפקט"

msgctxt "#30007"
msgid "Slide"
msgstr "מצגת"

msgctxt "#30008"
msgid "Crossfade"
msgstr "התמזגות"

msgctxt "#30009"
msgid "Pan and zoom"
msgstr "תקריב וחיתוך מסך"

msgctxt "#30010"
msgid "Amount of seconds to display each image"
msgstr "כמות השניות כדי להציג כל תמונה"

msgctxt "#30011"
msgid "Scale images fullscreen"
msgstr "הגדל תמונות למסף מלא"

msgctxt "#30012"
msgid "Display image name"
msgstr "הצג שם תמונה"

msgctxt "#30013"
msgid "Disabled"
msgstr "מנוטרל"

msgctxt "#30014"
msgid "Use filename"
msgstr "השתמש בשם קובץ"

msgctxt "#30015"
msgid "Use foldername"
msgstr "השתמש בשם תיקיה"

msgctxt "#30016"
msgid "Display images in random order"
msgstr "הצגת התמונות בסדר אקראי"

msgctxt "#30017"
msgid "Use full path"
msgstr "להשתמש בנתיב המלא"

# empty string with id 30017
msgctxt "#30018"
msgid "Use folder- and filename"
msgstr "להשתמש בתיקייה- ובשם הקובץ"

# empty strings from id 30019 to 30020
msgctxt "#30021"
msgid "Display picture date"
msgstr "הצג תאריך תמונה"

msgctxt "#30022"
msgid "Display additional picture tags"
msgstr "הצג תוויות תמונה נוספות"

msgctxt "#30023"
msgid "Display music info during audio playback"
msgstr "הצג פרטי מוזיקה בזמן ניגון אודיו"

msgctxt "#30024"
msgid "Basic"
msgstr "בסיסי"

msgctxt "#30025"
msgid "Additional"
msgstr "נוסף"

msgctxt "#30026"
msgid "Resume slideshow from last position"
msgstr "להמשיך את המצגת מאין שהופסקה"

# empty strings from id 30027 to 30028
msgctxt "#30029"
msgid "Display background picture"
msgstr "הצג תמונת רקע"

msgctxt "#30030"
msgid "Use vignette overlay"
msgstr ""
